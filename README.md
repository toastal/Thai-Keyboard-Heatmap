# Keyboard Heatmap
This repository is fork from [pa7/Keyboard-Heatmap](https://github.com/pa7/Keyboard-Heatmap.git) on GitHub.

Currently support keyboard layouts:
- IKBAEB-th (Ours (-w-;) )
- Kedmanee TIS-620.2538
- Kedmanee TIS-620.2536
- Kedmanee Rheinmetall
- Pattajoti X11
- Pattajoti Nectec
- Pattajoti Thanakan
- Underwood
- McFARLAND



## License
The Keyboard Heatmap is licensed under the [MIT](http://www.opensource.org/licenses/mit-license.php "") and [Beerware](http://en.wikipedia.org/wiki/Beerware "") License.

## Contact
If you have questions to ask original creater on whatever digital medium you prefer.
- [@patrickwied](http://twitter.com/#!/patrickwied "on twitter") 
- [www.patrick-wied.at](http://www.patrick-wied.at "on his website")

But for Thai layouts.
- [@s2hanano](https://pleroma.in.th/s2hanano "on Pleroma Fediverse") on Pleroma Fediverse.

Try it here:https://sahabandha.gitlab.io/Thai-Keyboard-Heatmap 